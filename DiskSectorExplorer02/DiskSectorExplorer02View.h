// DiskSectorExplorer02View.h : CDiskSectorExplorer02View 类的接口
//


#pragma once
#include "DiskSectorExplorer02Doc.h"




class CDiskSectorExplorer02View : public CFormView
{
protected: // 仅从序列化创建
	CDiskSectorExplorer02View();
	DECLARE_DYNCREATE(CDiskSectorExplorer02View)

public:
	enum{ IDD = IDD_DISKSECTOREXPLORER02_FORM };

// 属性
public:
	CDiskSectorExplorer02Doc* GetDocument() const;

// 操作
public:
	void SetPartitionName(CString partitionName);
// 重写
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
	virtual void OnInitialUpdate(); // 构造后第一次调用

// 实现
public:
	virtual ~CDiskSectorExplorer02View();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// 生成的消息映射函数
protected:
	DECLARE_MESSAGE_MAP()
	virtual void OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/);
};

#ifndef _DEBUG  // DiskSectorExplorer02View.cpp 中的调试版本
inline CDiskSectorExplorer02Doc* CDiskSectorExplorer02View::GetDocument() const
   { return reinterpret_cast<CDiskSectorExplorer02Doc*>(m_pDocument); }
#endif


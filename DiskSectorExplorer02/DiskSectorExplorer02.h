// DiskSectorExplorer02.h : DiskSectorExplorer02 应用程序的主头文件
//
#pragma once

#ifndef __AFXWIN_H__
	#error "在包含此文件之前包含“stdafx.h”以生成 PCH 文件"
#endif

#include "resource.h"       // 主符号


// CDiskSectorExplorer02App:
// 有关此类的实现，请参阅 DiskSectorExplorer02.cpp
//

class CDiskSectorExplorer02App : public CWinApp
{
public:
	CDiskSectorExplorer02App();


// 重写
public:
	virtual BOOL InitInstance();

// 实现
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
};

extern CDiskSectorExplorer02App theApp;
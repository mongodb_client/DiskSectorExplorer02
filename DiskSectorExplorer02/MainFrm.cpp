// MainFrm.cpp : CMainFrame 类的实现
//

#include "stdafx.h"
#include "DiskSectorExplorer02.h"
#include "DiskPartitionView.h"

#include "MainFrm.h"
#include "DiskSectorExplorer02View.h"
#include "HexEditView.h"
#include "AssignSectorDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	ON_WM_CREATE()
	ON_MESSAGE(WM_DiskPartitionView_2_MainFrame_PartitionChanged,&CMainFrame::OnSelectedPartitionChanged)
	ON_COMMAND(MENU_SECTOR_FIRST, &CMainFrame::OnSectorFirst)
	ON_COMMAND(MENU_SECTOR_PREV, &CMainFrame::OnSectorPrev)
	ON_COMMAND(MENU_SECTOR_NEXT, &CMainFrame::OnSectorNext)
	ON_COMMAND(MENU_SECTOR_LAST, &CMainFrame::OnSectorLast)
	ON_COMMAND(MENU_SECTOR_ASSIGN, &CMainFrame::OnSectorAssign)
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // 状态行指示器
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

/**
 * \brief 获取主框架窗口
 */
CMainFrame* AfxGetMainFrame()
{
	CWnd* mainWnd=AfxGetApp()->m_pMainWnd;
	CMainFrame * pFrame = dynamic_cast<CMainFrame*>(mainWnd);
	ASSERT( !pFrame || pFrame->IsKindOf(RUNTIME_CLASS(CMainFrame)));
	return pFrame;
}


// CMainFrame 构造/析构

CMainFrame::CMainFrame()
{
	// TODO: 在此添加成员初始化代码
	m_wndDiskPartitionView=NULL;
	m_wndSectorView=NULL;
	m_wndHexDataView=NULL;
}

CMainFrame::~CMainFrame()
{
}


int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("未能创建工具栏\n");
		return -1;      // 未能创建
	}

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("未能创建状态栏\n");
		return -1;      // 未能创建
	}

	// TODO: 如果不需要工具栏可停靠，则删除这三行
	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndToolBar);
	GetMenu()->EnableMenuItem(MENU_SECTOR_FIRST,MF_DISABLED);
	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: 在此处通过修改
	//  CREATESTRUCT cs 来修改窗口类或样式

	return TRUE;
}


// CMainFrame 诊断

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG


// CMainFrame 消息处理程序



//创建拆分视图
BOOL CMainFrame::OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext)
{
	// TODO: 在此添加专用代码和/或调用基类
	//客户矩形区
	CRect clientRect;
	GetClientRect(&clientRect);
	CSize czLeftPane(clientRect.Width()/4,clientRect.Height());
	CSize czRightPane(clientRect.Width()*2/4,clientRect.Height());
	//静态拆分为一行三列
	if (!m_wndSplitterV.CreateStatic(this,1,3))
	{
		return FALSE;
	}
	//第一列
	if (!m_wndSplitterV.CreateView(0,0,RUNTIME_CLASS(CDiskPartitionView),CSize(czLeftPane.cx/2,czLeftPane.cy/2),pContext))
	{
		return FALSE;
	}
	//第三列
	if (!m_wndSplitterV.CreateView(0,1,RUNTIME_CLASS(CDiskSectorExplorer02View),czLeftPane,pContext))
	{
		return FALSE;
	}
	//第二列
	if (!m_wndSplitterV.CreateView(0,2,RUNTIME_CLASS(CHexEditView),czRightPane,pContext))
	{
		return FALSE;
	}
	//初始化视图成员变量
	m_wndDiskPartitionView=(CDiskPartitionView*)m_wndSplitterV.GetPane(0,0);
	m_wndSectorView=(CDiskSectorExplorer02View*)m_wndSplitterV.GetPane(0,1);
	m_wndHexDataView=(CHexEditView*)m_wndSplitterV.GetPane(0,2);
	this->SetActiveView(m_wndDiskPartitionView);
	return TRUE;
}
//如果选择的分区发生变化，则响应此函数
LRESULT CMainFrame::OnSelectedPartitionChanged( WPARAM wParam,LPARAM lParam )
{
	//将消息的内容传给子窗口视图:m_wndSectorView
	//打开该磁盘的第一个扇区，并显示
	CDocument* doc=GetActiveDocument();
	CDiskSectorExplorer02Doc* pDoc=reinterpret_cast<CDiskSectorExplorer02Doc*>(doc);
	pDoc->SetDisk((LPTSTR)wParam,(int)lParam);
	m_wndHexDataView->SetData(pDoc->m_pData,pDoc->dwBytesPerSector);
	if (lParam==CDiskPartitionView::LOGICAL_PARTITION)
	{
		pDoc->UpdateAllViews(m_wndHexDataView);
	}
	//pDoc->SendInitialUpdate();
	return 0;
}

//将消息交给视图自己处理
void CMainFrame::OnSectorFirst()
{
	// TODO: 在此添加命令处理程序代码
	SetSelectedSector(CHexEditView::SECTOR_FIRST);
	//::PostMessage(m_wndHexDataView->GetSafeHwnd(),MAINFRAME_2_HEXEDITVIEW_SECTOR_SELECTED_CHANGED,0,CHexEditView::SECTOR_FIRST);
}

void CMainFrame::OnSectorPrev()
{
	// TODO: 在此添加命令处理程序代码
	SetSelectedSector(CHexEditView::SECTOR_PREV);
	//::PostMessage(m_wndHexDataView->GetSafeHwnd(),MAINFRAME_2_HEXEDITVIEW_SECTOR_SELECTED_CHANGED,0,CHexEditView::SECTOR_PREV);
}

void CMainFrame::OnSectorNext()
{
	// TODO: 在此添加命令处理程序代码
	SetSelectedSector(CHexEditView::SECTOR_NEXT);
	//::PostMessage(m_wndHexDataView->GetSafeHwnd(),MAINFRAME_2_HEXEDITVIEW_SECTOR_SELECTED_CHANGED,0,CHexEditView::SECTOR_NEXT);
}

void CMainFrame::OnSectorLast()
{
	// TODO: 在此添加命令处理程序代码
	SetSelectedSector(CHexEditView::SECTOR_LAST);
	//::PostMessage(m_wndHexDataView->GetSafeHwnd(),MAINFRAME_2_HEXEDITVIEW_SECTOR_SELECTED_CHANGED,0,CHexEditView::SECTOR_LAST);
}
void CMainFrame::SetSelectedSector(LPARAM lParam)
{
	CDocument* doc=GetActiveDocument();
	CDiskSectorExplorer02Doc* pDoc=reinterpret_cast<CDiskSectorExplorer02Doc*>(doc);
	switch(lParam)
	{
	case CHexEditView::SECTOR_FIRST:
		{
			m_wndHexDataView->m_currentSector=0;
			break;
		}

	case CHexEditView::SECTOR_NEXT:
		{
			m_wndHexDataView->m_currentSector++;
			break;
		}
	case CHexEditView::SECTOR_PREV:
		{
			m_wndHexDataView->m_currentSector--;
			break;
		}
	case CHexEditView::SECTOR_LAST:
		{
			m_wndHexDataView->m_currentSector=pDoc->dwSectors-1;
			break;
		}
	}
	pDoc->ReadData(m_wndHexDataView->m_currentSector*pDoc->dwBytesPerSector,pDoc->dwBytesPerSector);
	m_wndHexDataView->SetData(pDoc->m_pData,pDoc->dwBytesPerSector);
}
void CMainFrame::OnSectorAssign()
{
	// TODO: 在此添加命令处理程序代码
	CAssignSectorDlg assignSectorDlg;
//	assignSectorDlg.DoModal();
	if (assignSectorDlg.DoModal()==IDOK)
	{
		int dwAssignedSector=assignSectorDlg.m_dwAssignedSector;
		m_wndHexDataView->m_currentSector=dwAssignedSector-1;
		SetSelectedSector(CHexEditView::SECTOR_NEXT);
	}
}

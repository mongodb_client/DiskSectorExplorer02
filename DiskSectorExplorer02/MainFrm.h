// MainFrm.h : CMainFrame 类的接口
//


#pragma once

//自定义消息
//消息格式：WM_消息源_2_消息接收者_消息内容
#define WM_DiskPartitionView_2_MainFrame_PartitionChanged WM_USER+101


class CDiskPartitionView;
class CDiskSectorExplorer02View;
class CHexEditView;

class CMainFrame : public CFrameWnd
{
	
protected: // 仅从序列化创建
	CMainFrame();
	DECLARE_DYNCREATE(CMainFrame)

// 属性
public:
	CSplitterWnd m_wndSplitterV;//垂直拆分
	CSplitterWnd m_wndSplitterH;//水平拆分
	CDiskPartitionView* m_wndDiskPartitionView;//分区列表
	CDiskSectorExplorer02View* m_wndSectorView;//扇区浏览
	CHexEditView* m_wndHexDataView;//数据显示
// 操作
public:

// 重写
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

// 实现
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // 控件条嵌入成员
	CStatusBar  m_wndStatusBar;
	CToolBar    m_wndToolBar;

// 生成的消息映射函数
protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	DECLARE_MESSAGE_MAP()
	virtual BOOL OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext);
	//lParam为文件句柄
	LRESULT OnSelectedPartitionChanged(WPARAM wParam,LPARAM lParam);
public:
	afx_msg void OnSectorFirst();
	afx_msg void OnSectorPrev();
	afx_msg void OnSectorNext();
	afx_msg void OnSectorLast();
	void SetSelectedSector(LPARAM lParam);
	afx_msg void OnSectorAssign();
};
CMainFrame* AfxGetMainFrame();



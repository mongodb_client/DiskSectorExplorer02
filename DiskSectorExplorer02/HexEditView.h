#pragma once
#include "DiskSectorExplorer02Doc.h"


// CHexEditView 视图
#define MAINFRAME_2_HEXEDITVIEW_SECTOR_SELECTED_CHANGED WM_USER+102

class CHexEditView : public CEditView
{
	DECLARE_DYNCREATE(CHexEditView)
public:
	enum EDITMODE{
		EDIT_NONE,
		EDIT_ASCII,
		EDIT_HIGH,
		EDIT_LOW
	};
	enum SECTOR_SELECT{
		SECTOR_FIRST,
		SECTOR_PREV,
		SECTOR_NEXT,
		SECTOR_LAST
	};
	//公有属性
public:
	LPBYTE m_pData;//数据缓冲区指针
	int m_length;//数据大小
	int m_currentSector;//当前记区号
	int m_topindex;//代表了第一行第一个字节在文中的偏移量
	int m_currentAddress;//当前光标所在位置
	EDITMODE m_currentMode;
	int m_selStart;
	int m_selEnd;

	int m_bpr;//每行字节数
	int m_lpp;//每页行数
	BOOL m_bShowAddress;
	BOOL m_bShowAscii;
	BOOL m_bShowHex;
	BOOL m_bAddressIsWide;

	BOOL m_bNoAddressChange;
	BOOL m_bHalfPage;
	CFont m_Font;
	int m_lineHeight;
	int m_nullWidth;
	BOOL m_bUpdate;
	int m_offHex,
		m_offAscii,
		m_offAddress;
	CPoint m_editPos;
public:
	void ResetPos(BOOL b=TRUE);
	int GetSelLength();
	int GetData(LPBYTE p,int len);
	void SetData(LPBYTE p,int len);
	CSize GetSel(void);
	void SetSel(int s,int e);
	void SetBPR(int bpr);
	void SetOptions(BOOL a,BOOL h,BOOL c,BOOL w);
protected:
	void ScrollIntoView(int p);
	void RepositionCaret(int p);
	void Move(int x,int y);
	inline BOOL IsSelected(void);
	void UpdateScrollbars(void);
	void CreateEditCaret(void);
	void CreateAddressCaret(void);
	CPoint CalcPos(int x,int y);
	void SelInsert(int s,int l);
	void SelDelete(int s,int e);
	inline void NormalizeSel(void);
	afx_msg void OnContextMenu(CWnd*, CPoint point);
	
protected:
	CHexEditView();           // 动态创建所使用的受保护的构造函数
	virtual ~CHexEditView();

public:
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	DECLARE_MESSAGE_MAP()

	//消息处理函数
protected:
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg void OnPaint();
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg UINT OnGetDlgCode();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	//编辑操作响应函数
	afx_msg void OnEditClear();
	afx_msg void OnEditCopy();
	afx_msg void OnEditCut();
	afx_msg void OnEditPaste();
	afx_msg void OnEditSelectAll();
	afx_msg void OnEditUndo();
	CDiskSectorExplorer02Doc* GetDocument();
protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/);
public:
	LRESULT OnSectorSelectedChanged(WPARAM wParam,LPARAM lParam);
};



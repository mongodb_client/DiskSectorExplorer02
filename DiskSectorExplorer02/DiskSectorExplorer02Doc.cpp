// DiskSectorExplorer02Doc.cpp : CDiskSectorExplorer02Doc 类的实现
//

#include "stdafx.h"
#include "DiskSectorExplorer02.h"

#include "DiskSectorExplorer02Doc.h"
#include "DiskPartitionView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CDiskSectorExplorer02Doc

IMPLEMENT_DYNCREATE(CDiskSectorExplorer02Doc, CDocument)

BEGIN_MESSAGE_MAP(CDiskSectorExplorer02Doc, CDocument)
END_MESSAGE_MAP()


// CDiskSectorExplorer02Doc 构造/析构

CDiskSectorExplorer02Doc::CDiskSectorExplorer02Doc()
{
	// TODO: 在此添加一次性构造代码
	m_hDisk=INVALID_HANDLE_VALUE;
	m_pData=NULL;
}

CDiskSectorExplorer02Doc::~CDiskSectorExplorer02Doc()
{
}

BOOL CDiskSectorExplorer02Doc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: 在此添加重新初始化代码
	// (SDI 文档将重用该文档)

	return TRUE;
}




// CDiskSectorExplorer02Doc 序列化

void CDiskSectorExplorer02Doc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: 在此添加存储代码
	}
	else
	{
		// TODO: 在此添加加载代码
	}
}


// CDiskSectorExplorer02Doc 诊断

#ifdef _DEBUG
void CDiskSectorExplorer02Doc::AssertValid() const
{
	CDocument::AssertValid();
}

void CDiskSectorExplorer02Doc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}

int CDiskSectorExplorer02Doc::ReadData(DWORD iStart,DWORD nCount)
{
	free(m_pData);
	m_pData=(LPBYTE)malloc(nCount);
	::SetFilePointer(m_hDisk,iStart,0,FILE_BEGIN);
	DWORD readBytes=0;
	::ReadFile(m_hDisk,m_pData,nCount,&readBytes,NULL);
	return readBytes;
}

void CDiskSectorExplorer02Doc::SetDisk( CString strPartitionName,int diskType)
{
	//先关闭文件句柄
	m_strPartitionName=strPartitionName;
	if (m_strPartitionName.IsEmpty())
	{
		return ;
	}
	CString diskPath;
	//区分是逻辑磁盘还是物理硬盘
	if (diskType==CDiskPartitionView::LOGICAL_PARTITION)
	{
		diskPath.Format(_T("\\\\.\\%s"),strPartitionName);
		diskPath.Delete(diskPath.ReverseFind(_T('\\')),1);
		//获得卷信息
		GetVolumeInformation(m_strPartitionName,lpVolumeNameBuffer,200,&dwVolumeSerialNumber,
			&dwMaximumComponentLength,&dwFileSystemFlags,lpFileSystemNameBuffer,50);
		//获得磁盘参数信息
		GetDiskFreeSpace(m_strPartitionName,&dwSectorsPerCluster,&dwBytesPerSector,&dwFreeClusters,&dwClusters);
		GetDiskFreeSpaceEx(m_strPartitionName,&uliFreeBytesAvailableToCaller,&uliTotalNumberOfBytes,&uliTotalNumberOfFreeBytes);
		m_uliPartitionSize=uliTotalNumberOfBytes;

		dwSectors=dwSectorsPerCluster*dwClusters;
		LONG usedSpaceLowPart=uliTotalNumberOfBytes.LowPart-uliTotalNumberOfFreeBytes.LowPart;
		LONG usedSpaceHighPart=uliTotalNumberOfBytes.HighPart-uliTotalNumberOfFreeBytes.HighPart;
		if (usedSpaceLowPart<0)
		{
			uliTotalNumberOfUsedBytes.LowPart=uliTotalNumberOfFreeBytes.LowPart-uliTotalNumberOfBytes.LowPart;
			uliTotalNumberOfUsedBytes.HighPart=usedSpaceHighPart-1;
		}else{
			uliTotalNumberOfUsedBytes.LowPart=usedSpaceLowPart;
			uliTotalNumberOfUsedBytes.HighPart=usedSpaceHighPart;
		}
	}
	else if (diskType==CDiskPartitionView::PHYSICAL_DISK)
	{
		diskPath.Format(_T("\\\\.\\PhysicalDrive%s"),strPartitionName);
		diskPath.Delete(diskPath.Find(_T("Disk_"),16),5);
		dwBytesPerSector=512;
	}
	if (m_hDisk!=INVALID_HANDLE_VALUE)
	{
		CloseHandle(m_hDisk);
	}
	//创建文件句柄
	m_hDisk=::CreateFile(diskPath,
		GENERIC_READ,FILE_SHARE_WRITE,0,OPEN_EXISTING,0,0);
	if (INVALID_HANDLE_VALUE==m_hDisk)
	{
		AfxMessageBox(diskPath);
	}

	ReadData(0,dwBytesPerSector);
}

#endif //_DEBUG


// CDiskSectorExplorer02Doc 命令

// DiskPartitionView.cpp : 实现文件
//

#include "stdafx.h"
#include "DiskSectorExplorer02.h"
#include "DiskPartitionView.h"
#include "MainFrm.h"




// CDiskPartitionView

IMPLEMENT_DYNCREATE(CDiskPartitionView, CTreeView)

CDiskPartitionView::CDiskPartitionView()
{

}

CDiskPartitionView::~CDiskPartitionView()
{
}

BEGIN_MESSAGE_MAP(CDiskPartitionView, CTreeView)
	ON_NOTIFY_REFLECT(TVN_SELCHANGED, &CDiskPartitionView::OnTvnSelchanged)
END_MESSAGE_MAP()


// CDiskPartitionView 诊断

#ifdef _DEBUG
void CDiskPartitionView::AssertValid() const
{
	CTreeView::AssertValid();
}

#ifndef _WIN32_WCE
void CDiskPartitionView::Dump(CDumpContext& dc) const
{
	CTreeView::Dump(dc);
}
#endif
#endif //_DEBUG

// CDiskPartitionView 消息处理程序
//视图第一次初始化时也会调用此函数
void CDiskPartitionView::OnInitialUpdate()
{
	CTreeView::OnInitialUpdate();
	if (GetTreeCtrl().GetRootItem()!=NULL)
	{
		return ;
	}
	//初始化树形控件
	HTREEITEM root1,root2;
	root1=GetTreeCtrl().InsertItem(_T("分区列表"));
	DWORD needDriveStringBufSize=GetLogicalDriveStrings(0,NULL);
	LPTSTR lpDriveStrings=(LPTSTR)HeapAlloc(GetProcessHeap(),0,needDriveStringBufSize*sizeof(TCHAR));
	GetLogicalDriveStrings(needDriveStringBufSize,lpDriveStrings);
	int nextNullIndex=0,nullIndex=0;
	while (lpDriveStrings[0]!=_T('\0'))
	{
		HTREEITEM partitionItem=
			GetTreeCtrl().InsertItem(lpDriveStrings,root1);
		GetTreeCtrl().SetItemData(partitionItem,LOGICAL_PARTITION);
		lpDriveStrings=_tcschr(lpDriveStrings,0)+1;
	}

	root2=GetTreeCtrl().InsertItem(_T("硬盘列表"));
	DWORD* pDiskNumbers[MAX_DEVICE];
	int nDevices=GetAllPresentDisks(pDiskNumbers);
	CString diskNumber;
	for (int i=0;i<nDevices;i++)
	{
		diskNumber.Format(_T("Disk_%d"),*(*pDiskNumbers + i));
		HTREEITEM diskItem=GetTreeCtrl().InsertItem(diskNumber,root2);
		GetTreeCtrl().SetItemData(diskItem,PHYSICAL_DISK);
	}

}

BOOL CDiskPartitionView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: 在此添加专用代码和/或调用基类
	cs.style|=(TVS_HASLINES|TVS_HASBUTTONS|TVS_LINESATROOT|TVS_SINGLEEXPAND);
	return CTreeView::PreCreateWindow(cs);
}

void CDiskPartitionView::OnTvnSelchanged(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);
	
	// TODO: 在此添加控件通知处理程序代码
	//读取磁盘
	//
	// load file, ...
	//
	TV_ITEM t_SelectedItem=pNMTreeView->itemNew;
	HTREEITEM t_SelTreeItem=t_SelectedItem.hItem;
	if (t_SelTreeItem==GetTreeCtrl().GetRootItem())
	{
		return ;
	}
	m_strSelectedDiskName=GetTreeCtrl().GetItemText(t_SelTreeItem);
	DWORD nodeType=GetTreeCtrl().GetItemData(t_SelTreeItem);
	//向主窗口发送消息
	::PostMessage(AfxGetMainFrame()->m_hWnd,WM_DiskPartitionView_2_MainFrame_PartitionChanged,
		(WPARAM)m_strSelectedDiskName.GetBuffer(32),(LPARAM)nodeType);
	*pResult = 0;
}

DWORD CDiskPartitionView::GetDevicePath( LPGUID lpGuid,TCHAR ** pszDevicePath )
{
	HDEVINFO hDevInfoSet;
	SP_DEVICE_INTERFACE_DATA ifdata;
	PSP_DEVICE_INTERFACE_DETAIL_DATA pDetail;
	DWORD nCount;
	BOOL result;

	//get a handle to a device information set 
	hDevInfoSet = SetupDiGetClassDevs(
		lpGuid,      // class GUID 
		NULL,        // Enumerator
		NULL,        // hwndParent
		DIGCF_PRESENT | DIGCF_DEVICEINTERFACE    // present devices
		);

	//fail...
	if (hDevInfoSet == INVALID_HANDLE_VALUE)
	{
		fprintf(stderr, "IOCTL_STORAGE_GET_DEVICE_NUMBER Error: %ld\n", GetLastError());
		return (DWORD)-1;
	}

	pDetail = (PSP_DEVICE_INTERFACE_DETAIL_DATA)malloc(INTERFACE_DETAIL_SIZE);
	if (pDetail == NULL)
	{
		return (DWORD)-1;
	}
	pDetail->cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);

	nCount = 0;
	result = TRUE;

	// device index = 0, 1, 2... test the device interface one by one
	while (result)
	{
		ifdata.cbSize = sizeof(ifdata);

		//enumerates the device interfaces that are contained in a device information set
		result = SetupDiEnumDeviceInterfaces(
			hDevInfoSet,     // DeviceInfoSet
			NULL,            // DeviceInfoData
			lpGuid,          // GUID
			nCount,   // MemberIndex
			&ifdata        // DeviceInterfaceData
			);
		if (result)
		{
			// get details about a device interface
			result = SetupDiGetDeviceInterfaceDetail(
				hDevInfoSet,    // DeviceInfoSet
				&ifdata,        // DeviceInterfaceData
				pDetail,        // DeviceInterfaceDetailData
				INTERFACE_DETAIL_SIZE,    // DeviceInterfaceDetailDataSize
				NULL,           // RequiredSize
				NULL          // DeviceInfoData
				);
			int n=GetLastError();
			if (result)
			{
				// copy the path to output buffer
				_tcscpy(
					pszDevicePath[nCount],
					pDetail->DevicePath
					);
				//printf("%s\n", pDetail->DevicePath);
				nCount++;
			}
		}
	}

	free(pDetail);
	(void)SetupDiDestroyDeviceInfoList(hDevInfoSet);

	return nCount;

}

DWORD CDiskPartitionView::GetAllPresentDisks( DWORD** ppDisks )
{
	TCHAR *szDevicePath[MAX_DEVICE];        // device path
	DWORD nDevice;
	HANDLE hDevice;
	STORAGE_DEVICE_NUMBER number;
	BOOL result;
	DWORD readed;
	WORD i, j;

	for (i = 0; i < MAX_DEVICE; i++)
	{
		szDevicePath[i] = (TCHAR *)malloc(INTERFACE_DETAIL_SIZE);
		if (NULL == szDevicePath[i])
		{
			for (j = 0; j < i; j++)
			{
				free(szDevicePath[i]);
			}
			return (DWORD)-1;
		}
	}

	// get the device paths
	nDevice = GetDevicePath(const_cast<LPGUID>(&GUID_DEVINTERFACE_DISK), szDevicePath);
	if ((DWORD)-1 == nDevice)
	{
		for (i = 0; i < MAX_DEVICE; i++)
		{
			free(szDevicePath[i]);
		}
		return (DWORD)-1;
	}

	*ppDisks = (DWORD *)malloc(sizeof(DWORD) * nDevice);
	// get the disk's physical number one by one
	for (i = 0; i < nDevice; i++)
	{
		hDevice = CreateFile(
			szDevicePath[i], // drive to open
			GENERIC_READ | GENERIC_WRITE,     // access to the drive
			FILE_SHARE_READ | FILE_SHARE_WRITE, //share mode
			NULL,             // default security attributes
			OPEN_EXISTING,    // disposition
			0,                // file attributes
			NULL            // do not copy file attribute
			);
		if (hDevice == INVALID_HANDLE_VALUE) // cannot open the drive
		{
			for (j = 0; j < MAX_DEVICE; j++)
			{
				free(szDevicePath[j]);
			}
			free(*ppDisks);
			fprintf(stderr, "CreateFile() Error: %ld\n", GetLastError());
			return DWORD(-1);
		}
		result = DeviceIoControl(
			hDevice,                // handle to device
			IOCTL_STORAGE_GET_DEVICE_NUMBER, // dwIoControlCode
			NULL,                            // lpInBuffer
			0,                               // nInBufferSize
			&number,           // output buffer
			sizeof(number),         // size of output buffer
			&readed,       // number of bytes returned
			NULL      // OVERLAPPED structure
			);
		if (!result) // fail
		{
			fprintf(stderr, "IOCTL_STORAGE_GET_DEVICE_NUMBER Error: %ld\n", GetLastError());
			for (j = 0; j < MAX_DEVICE; j++)
			{
				free(szDevicePath[j]);
			}
			free(*ppDisks);
			(void)CloseHandle(hDevice);
			return (DWORD)-1;
		}
		*(*ppDisks + i) = number.DeviceNumber;

		(void)CloseHandle(hDevice);
	}
	for (i = 0; i < MAX_DEVICE; i++)
	{
		free(szDevicePath[i]);
	}
	return nDevice;

}

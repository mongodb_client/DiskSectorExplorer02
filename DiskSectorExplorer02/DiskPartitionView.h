#pragma once
#include <afxcview.h>
#include <SetupAPI.h>
#include <devguid.h>
#include <cfgmgr32.h>
#include <winioctl.h>
#include <ntddscsi.h>
#include <NtDDNdis.h>


typedef struct _SCSI_PASS_THROUGH_WITH_BUFFERS {
	SCSI_PASS_THROUGH Spt;
	ULONG             Filler;      // realign buffers to double word boundary
	UCHAR             SenseBuf[32];
	UCHAR             DataBuf[512];
} SCSI_PASS_THROUGH_WITH_BUFFERS, *PSCSI_PASS_THROUGH_WITH_BUFFERS;

// BOOL GetRegistryProperty( HDEVINFO DevInfo, DWORD Index );
// BOOL GetDeviceProperty(HDEVINFO IntDevInfo, DWORD Index );
// CDiskPartitionView 视图
//磁盘分区列表视图
class CDiskPartitionView : public CTreeView
{
	DECLARE_DYNCREATE(CDiskPartitionView)
	enum{
	INTERFACE_DETAIL_SIZE=256,
	MAX_DEVICE=256,
	};
	enum{
		LOGICAL_PARTITION=1,
		PHYSICAL_DISK=2,
	};
protected:
	CDiskPartitionView();           // 动态创建所使用的受保护的构造函数
	virtual ~CDiskPartitionView();
    CString m_strSelectedDiskName;
public:
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	DECLARE_MESSAGE_MAP()
public:
	virtual void OnInitialUpdate();
	//获取设备路径
	DWORD GetDevicePath(LPGUID lpGuid,TCHAR ** pszDevicePath);
	DWORD GetAllPresentDisks(DWORD** ppDisks);
protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
public:
	//选择发生改变
	afx_msg void OnTvnSelchanged(NMHDR *pNMHDR, LRESULT *pResult);
};



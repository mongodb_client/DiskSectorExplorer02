// DiskSectorExplorer02Doc.h : CDiskSectorExplorer02Doc 类的接口
//


#pragma once


class CDiskSectorExplorer02Doc : public CDocument
{
protected: // 仅从序列化创建
	CDiskSectorExplorer02Doc();
	DECLARE_DYNCREATE(CDiskSectorExplorer02Doc)

// 属性
public:
	HANDLE m_hDisk;//磁盘文件句柄
	LPBYTE m_pData;//数据指针
	CString m_strPartitionName;//分区名
	ULARGE_INTEGER m_uliPartitionSize;//分区大小
	TCHAR lpVolumeNameBuffer[200];
	DWORD dwVolumeSerialNumber,dwMaximumComponentLength;
	DWORD dwFileSystemFlags;
	TCHAR lpFileSystemNameBuffer[50];
	DWORD dwSectorsPerCluster,dwBytesPerSector;
	DWORD dwFreeClusters,dwClusters,dwSectors;
	ULARGE_INTEGER uliFreeBytesAvailableToCaller,uliTotalNumberOfBytes,uliTotalNumberOfFreeBytes;
	ULARGE_INTEGER uliTotalNumberOfUsedBytes;//这里要用大整数
// 操作
public:
	int ReadData(DWORD iStart,DWORD nCount);
	void SetDisk(CString strPartitionName,int diskType);
// 重写
public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);

// 实现
public:
	virtual ~CDiskSectorExplorer02Doc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// 生成的消息映射函数
protected:
	DECLARE_MESSAGE_MAP()
};



#pragma once


// CAssignSectorDlg 对话框

class CAssignSectorDlg : public CDialog
{
	DECLARE_DYNAMIC(CAssignSectorDlg)

public:
	CAssignSectorDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CAssignSectorDlg();

// 对话框数据
	enum { IDD = DIALOG_ASSIGN_SECTOR };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	// 指定的扇区号
	DWORD m_dwAssignedSector;
};

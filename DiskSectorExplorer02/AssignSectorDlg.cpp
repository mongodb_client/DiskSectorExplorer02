// AssignSectorDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "DiskSectorExplorer02.h"
#include "AssignSectorDlg.h"


// CAssignSectorDlg 对话框

IMPLEMENT_DYNAMIC(CAssignSectorDlg, CDialog)

CAssignSectorDlg::CAssignSectorDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CAssignSectorDlg::IDD, pParent)
	, m_dwAssignedSector(0)
{

}

CAssignSectorDlg::~CAssignSectorDlg()
{
}

void CAssignSectorDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, EDIT_SECTOR_ASSIGN, m_dwAssignedSector);
}


BEGIN_MESSAGE_MAP(CAssignSectorDlg, CDialog)
END_MESSAGE_MAP()


// CAssignSectorDlg 消息处理程序

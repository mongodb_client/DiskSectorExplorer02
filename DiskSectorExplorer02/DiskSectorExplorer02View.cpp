// DiskSectorExplorer02View.cpp : CDiskSectorExplorer02View 类的实现
//

#include "stdafx.h"
#include "DiskSectorExplorer02.h"

#include "DiskSectorExplorer02Doc.h"
#include "DiskSectorExplorer02View.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CDiskSectorExplorer02View

IMPLEMENT_DYNCREATE(CDiskSectorExplorer02View, CFormView)

BEGIN_MESSAGE_MAP(CDiskSectorExplorer02View, CFormView)
END_MESSAGE_MAP()

// CDiskSectorExplorer02View 构造/析构

CDiskSectorExplorer02View::CDiskSectorExplorer02View()
	: CFormView(CDiskSectorExplorer02View::IDD)
{
	// TODO: 在此处添加构造代码

}

CDiskSectorExplorer02View::~CDiskSectorExplorer02View()
{
}

void CDiskSectorExplorer02View::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
}

BOOL CDiskSectorExplorer02View::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: 在此处通过修改
	//  CREATESTRUCT cs 来修改窗口类或样式

	return CFormView::PreCreateWindow(cs);
}

void CDiskSectorExplorer02View::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	GetParentFrame()->RecalcLayout();
	ResizeParentToFit();
}


// CDiskSectorExplorer02View 诊断

#ifdef _DEBUG
void CDiskSectorExplorer02View::AssertValid() const
{
	CFormView::AssertValid();
}

void CDiskSectorExplorer02View::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CDiskSectorExplorer02Doc* CDiskSectorExplorer02View::GetDocument() const // 非调试版本是内联的
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CDiskSectorExplorer02Doc)));
	return (CDiskSectorExplorer02Doc*)m_pDocument;
}

#endif //_DEBUG


// CDiskSectorExplorer02View 消息处理程序

void CDiskSectorExplorer02View::OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/)
{
	// TODO: 在此添加专用代码和/或调用基类
	CDiskSectorExplorer02Doc* pDoc=GetDocument();
	if (pDoc->m_strPartitionName.IsEmpty())
	{
		return ;
	}
	SetDlgItemText(EDIT_DISK_NAME,pDoc->m_strPartitionName);
	SetDlgItemText(EDIT_VOLUME_NAME,pDoc->lpVolumeNameBuffer);
	CString VolumeSerialNumber;
	VolumeSerialNumber.Format(_T("%u"),pDoc->dwVolumeSerialNumber);
	SetDlgItemText(EDIT_SERIAL_NUMBER,VolumeSerialNumber);
	SetDlgItemInt(EDIT_MAX_FILENAME,pDoc->dwMaximumComponentLength);
	SetDlgItemInt(EDIT_SYSTEM_FLAG,pDoc->dwFileSystemFlags);
	SetDlgItemText(EDIT_FILESYSTEM_NAME,pDoc->lpFileSystemNameBuffer);

	SetDlgItemInt(EDIT_SECTORS_PER_CLUSTER,pDoc->dwSectorsPerCluster);
	SetDlgItemInt(EDIT_BYTES_PER_SECTORS,pDoc->dwBytesPerSector);
	SetDlgItemInt(EDIT_TOTAL_CLUSTER,pDoc->dwClusters);
	SetDlgItemInt(EDIT_FREE_CLUSTER,pDoc->dwFreeClusters);
	CString space;
	space.Format(_T("%d%d"),pDoc->uliTotalNumberOfBytes.HighPart,pDoc->uliTotalNumberOfBytes.LowPart);
	SetDlgItemText(EDIT_TOTAL_SPACE,space);
	space.Format(_T("%d%d"),pDoc->uliTotalNumberOfFreeBytes.HighPart,pDoc->uliTotalNumberOfFreeBytes.LowPart);
	SetDlgItemText(EDIT_FREE_SPACE,space);
	
	space.Format(_T("%d%d"),pDoc->uliTotalNumberOfUsedBytes.HighPart,pDoc->uliTotalNumberOfUsedBytes.LowPart);
	SetDlgItemText(EDIT_USED_SPACE,space);
}

//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by DiskSectorExplorer02.rc
//
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDD_DISKSECTOREXPLORER02_FORM   101
#define IDR_MAINFRAME                   128
#define IDR_DiskSectorExploTYPE         129
#define CG_IDR_POPUP_HEX_EDIT           130
#define DIALOG_ASSIGN_SECTOR            131
#define EDIT_VOLUME_NAME                1000
#define EDIT_SERIAL_NUMBER              1001
#define EDIT_SECTOR_ASSIGN              1001
#define EDIT_MAX_FILENAME               1002
#define EDIT_SYSTEM_FLAG                1003
#define EDIT_FILESYSTEM_NAME            1004
#define EDIT_SECTORS_PER_CLUSTER        1005
#define EDIT_BYTES_PER_SECTORS          1006
#define EDIT_FREE_CLUSTER               1007
#define EDIT_TOTAL_CLUSTER              1008
#define EDIT_TOTAL_SPACE                1009
#define EDIT_FREE_SPACE                 1010
#define EDIT_USED_SPACE                 1011
#define EDIT_DISK_NAME                  1012
#define ID_32771                        32771
#define ID_32772                        32772
#define ID_32773                        32773
#define ID_32774                        32774
#define ID_32775                        32775
#define ID_32776                        32776
#define ID_                             32777
#define ID_32778                        32778
#define ID_32779                        32779
#define ID_32780                        32780
#define ID_32781                        32781
#define ID_32782                        32782
#define ID_32783                        32783
#define MENU_SECTOR_FIRST               32784
#define MENU_SECTOR_PREV                32785
#define MENU_SECTOR_NEXT                32786
#define MENU_SECTOR_LAST                32787
#define MENU_SECTOR_ASSIGN              32788
#define ID_BUTTON32795                  32795

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        132
#define _APS_NEXT_COMMAND_VALUE         32796
#define _APS_NEXT_CONTROL_VALUE         1002
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
